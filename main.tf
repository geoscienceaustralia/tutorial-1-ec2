provider "aws" {
  region = "ap-southeast-2"
}

terraform {
  backend "s3" {
    bucket     = "gadevs-tfstate"
    key        = "test/terraform-ec2.tfstate"
    region     = "ap-southeast-2"
    lock_table = "terraform-lock"
  }
}

resource "aws_instance" "www" {
  ami           = "ami-4d3b062e"
  instance_type = "t2.micro"

  tags = {
    Name = "terraform_ec2"
  }
}
